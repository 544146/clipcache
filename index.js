addEventListener('fetch', function(event) {
  event.respondWith(handleRequest(event));
});

async function handleRequest(event) {
  const request = event.request

  const original_url = new URL(request.url)
  const url_path_string = original_url.pathname.toString()

  const cache_url = new URL(`${B2PROXY_URL}${url_path_string}`)
  
  //const cache_key = new Request(cache_url, request)
  // const cache = caches.default
  // let response = await cache.match(cache_key)
  // if (response) {
  //     return response;
  // }

  let init = {
      method: request.method,
      headers: {}
  };

  //"range",
  const PROXY_HEADERS = ["accept",
      "accept-encoding",
      "accept-language",
      "cache-conrol",
      "dnt",
      "sec-fetch-dest",
      "sec-fetch-mode",
      "sec-fetch-site",
      "upgrade-insecure-requests",
      "user-agent"
  ];

  for (let name of PROXY_HEADERS) {
    let value = request.headers.get(name);
    if (value) {
      init.headers[name] = value;
    }
  }

  //fix for safari n shiz
  if (is_browser_mobile(request.headers.userAgent)) {
      init.headers['range'] = request.headers.get('range')
  }

  //then fetch from origin
  console.log(`Fetching url ${cache_url}...`)
  response = await fetch(cache_url, init)

  console.log("Creating response...")
  var altered_headers = new Headers(response);
  //altered_headers.set("content-type", "video/mp4")

  response = new Response(response.body, response)
  if (!response.ok) return new Response('404', {status: 404})

  //console.log(await cache.put(cacheKey, response.clone()))
  //console.log("Placing response in cache...")
  //event.waitUntil(cache.put(cache_key, response.clone()))
  return response
}

async function is_browser_mobile(userAgent) {
  return (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(userAgent))
}
